import React, { Component } from 'react'
import UpdateComponent from './HOCWithCounter'


 class HoverCounter extends Component {
     
     
    render() {

        //!destructuring form Hoc Of your Props
        const {count,clickCounter}=this.props
        return (
            <div>
                <h1 onMouseOver={clickCounter}>Hoverd {this.props.name} {count} times</h1>
            </div>
        )
    }
}

export default UpdateComponent(HoverCounter)
