import React, { Component } from 'react'

class Eventbind extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             Message:'hello'
        }
    }
    
 ClickHandler() {
     this.setState({
         Message:'GOOD Bye'
     })
    
} 
    render() {
        return (
            <div>
                <div>{this.state.Message}</div>
                <button onClick={this.ClickHandler.bind(this)}>Click ME</button>
            </div>
        )
    }
}

export default Eventbind
