import React from 'react'


/**
 * 
 * @returns data comes from anothe file list name 
 * !using Props 
 * ?this Component life include in listname file
 */
function PersonListRender({pro}) {
    return (
        <div>
            <h3>My Name is {pro.name} and I know { pro.skill}</h3>
        </div>
    )
}

export default PersonListRender
