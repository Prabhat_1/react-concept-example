import React from 'react'

const UpdateComponent=(OrignalComponent)=>{
    class NewComponent extends React.Component{

        constructor(props) {
            super(props)
        
            this.state = {
                 count:0
            }}
            clickCounter=()=>{
               this.setState(perState=>{
                   return{count:perState.count+1}
                       
                
               })
        }
       

        render(){
            return <OrignalComponent name="Prabhat"
                count={this.state.count}
                clickCounter={this.clickCounter}
            />

        }
    }
    return NewComponent
}
export default UpdateComponent