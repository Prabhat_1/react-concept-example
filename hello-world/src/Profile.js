import React from 'react'

function Profile(props)
{
    const press=()=>{
        alert("Heading Clicked");
    }
    console.log(props);

    return <div>
    <h1 onClick={press}>{props.name} Singh {props.heroname} </h1>
{props.children}
    </div>
}

export default Profile;