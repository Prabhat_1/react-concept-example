
import FRInput from './FRInput'

import React, { Component } from 'react'

export class FRParent extends Component {

    constructor(props) {
        super(props)
    
        this.inputRef=React.createRef();
    }
    clickHandler=()=>{
        this.inputRef.current.focus()
    }
    
    render() {
        return (
            <div>
                <FRInput ref={this.inputRef}/>
                <button onClick={this.clickHandler}>Focus ME</button>
            </div>
        )
    }
}

export default FRParent
