import React, { Component } from 'react'
import ChildComponent from './ChildComponent';
import ChildComp2 from './ChildComp2';

class ParentComponent extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             parentName:"Mukesh"
        }
        this.greetparent=this.greetparent.bind(this);
    }
    greetparent(child){
        alert(`hello ${this.state.parentName} from ${child}`);
    }
    render() {
        return (
            <div>
                <ChildComponent greetHandler={this.greetparent}/>
                <ChildComp2 greetHandler={this.greetparent}/>

            </div>
        )
    }
}

export default ParentComponent
