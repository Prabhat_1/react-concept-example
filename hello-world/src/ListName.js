import React from 'react'
import PersonListRender from './PersonListRender'

function ListName() {
    // const names=['Prabhat','Singh','Yadav','Love','Udita']
    const person=[
        {
            id:1,
            name:'prabhat',
            age:30,
            skill:'React'
        },
        {
            id:2,
            name:'Ankit',
            age:30,
            skill:'.net'
        },
        {
            id:3,
            name:'Nafar',
            age:30,
            skill:'DataBase'
        },
        {
            
            id:4,
            name:'Sadab',
            age:30,
            skill:'Angualr'
        }
    ]
    /*
    this is not best approch-----------------------the recomende way render the refactor the jsx in saprate coponent create new 
*/
    const personList=person.map(pname=>(
        <PersonListRender key ={pname.id} pro={pname}/>  /**
         * ?this is using pro as a name of prop name pass the pname map array into in it
         * ! we Can access person object us pname
         ** pass the data to parent to child define the child name in parent house then access chid parent property
          that's why we use personListREnder
         */
    ))
    return <div> {personList}   </div>
           
            
              
               
            

    
}

export default ListName
