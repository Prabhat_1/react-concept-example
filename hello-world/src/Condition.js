import React, { Component } from 'react'

export class Condition extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             UserLogedin:false
        }
    }
    
    render() {
        return this.state.UserLogedin ? (
            <div>Welcome Prabhat</div>
        ):(
            <div>Welcome Guest</div>
        )
        
    }
}

export default Condition
