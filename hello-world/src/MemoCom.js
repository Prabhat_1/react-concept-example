import React from 'react'

function MemoCom({props}) {
    console.log("Rendering memo")
    return (
        <div>
            {props}
        </div>
    )
}

export default React.memo(MemoCom)
