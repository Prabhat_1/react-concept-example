import React from 'react'

export default function ChildComp2(props) {
    return (
        <div>
            <button onClick={props.greetHandler}>Hello Btn2</button>
        </div>
    )
}
