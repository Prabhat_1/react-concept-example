import React from 'react'

function Hero({heroname}) {
    if(heroname==="Joker"){
        throw new Error("Not A Hero")
    }
    return (
        <div>
            {heroname}
        </div>
    )
}

export default Hero
