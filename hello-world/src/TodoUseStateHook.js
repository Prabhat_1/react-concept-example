import React,{useState} from 'react'



function TodoUseStateHook() {
const [userInput,setUserInput]=useState(' ');
const [userInputList,setUserInputList]=useState([])

const InputHandler=event=>{
setUserInput(event.target.value);
}
const AddHandler=()=>{
    setUserInputList(userInputList.concat(userInput));
}
    return (
        <div>
            <input type="text" placeholder="Todo" 
            onChange={InputHandler}
             value={userInput}/>
            <button onClick={AddHandler}>Add</button>
            <ul>
                {userInputList.map(todo=>(
                    <li key={todo.id}>{todo}</li>
                ))}
            </ul>
        </div>
    )
}

export default TodoUseStateHook
