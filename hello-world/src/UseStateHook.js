
import React,{useState} from 'react'
import CartItem from './CartItem';

function UseStateHook() {
      //!Declare a new state variable, which we'll call "count"
    const [count,setcount]=useState('Prabhat');
    const [apples,setApples]=useState(0);
    const [banana,setBanana]=useState(0);
    const [orange,setOrange]=useState(0);

   const clickVal=()=>{
        setcount('You Click On Button')
    }
  
  
    return (
        <div>
           <p className="Para">{count}</p>
          <CartItem name='Apple'
            price={45}
           quantity={apples}
           
            onClick={()=>setApples(prev=>prev+1)}
            remove={()=>{
                if(apples<=0){
                    setApples(0);
                }
                else{
                    setApples(apples-1);
                }
            }}   
           />
          <CartItem name='banana' quantity={orange}
          price={45}
              onClick={()=>setOrange(prev=>prev+1)}
            remove={()=>{
                if(orange<=0){
                    setOrange(0);
                }
                else{
                    setOrange(orange-1);
                }
            }}   
          />
          <CartItem name='orange'quantity={banana}
          price={45}
              onClick={()=>setBanana(prev=>prev+1)}
            remove={()=>{
                if(banana<=0){
                    setBanana(0);
                }
                else{
                    setBanana(banana-1);
                }
            }}   
          />
          <div>Total Item:{apples+banana+orange}</div>
            <div>Sum={45*apples}</div>
            <button onClick={clickVal}>Click </button>
        </div>
    )
}

export default UseStateHook
