import React,{useState,useEffect} from 'react'

function UseEffectHook() {
    const [counter, setcounter] = useState(0)
    //!change the Document title 
    useEffect(() => {
        document.title=`you Clicked ${counter} times`
    })
    return (
        <div>
            <button onClick={()=>setcounter(counter+1)}>Click{counter}</button>
        </div>
    )
}

export default UseEffectHook
