import React, { PureComponent } from 'react'

export class PureCompo extends PureComponent {

    render() {
        console.log("PureCalled")
        return (
            <div>
                pure Component {this.props.name}
            </div>
        )
    }
}

export default PureComponent
