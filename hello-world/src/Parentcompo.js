import React, { Component } from 'react'
import RegularCompo from './RegularCompo'
import { PureCompo } from './PureComponent';
import MemoCom from './MemoCom';

 class Parentcompo extends Component {
     constructor(props) {
         super(props)
     
         this.state = {
              name:'Visvash'
         }
     }
     componentDidMount(){
         setInterval(()=>{
             this.setState({
                 name:'Visvash'
             })
         },2000)
     }
     
    render() {
        console.log("Parent Comp Calll")
        return (
            <div>
                Parent Component
                {/* <RegularCompo name={this.state.name}/>
              <PureCompo name={this.state.name}/> */}
              <MemoCom name={this.state.name}/>
            </div>
        )
    }
}

export default Parentcompo
