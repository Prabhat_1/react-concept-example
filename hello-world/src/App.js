
import './App.css';
import Counter from './Counter';
import Profile from './Profile';
//import Card from './Card';
import Message from './Message'
import Eventbind from './Eventbind';
import ParentComponent from './ParentComponent';
import Condition from './Condition';
import ListName from './ListName';
import StyleSheet from './StyleSheet';
import Inline from './Inline';
import Form from './Form';
import Fragmentex from './Fragmentex';
import Table from './Table';
import { PureCompo } from './PureComponent';
import Parentcompo from './Parentcompo';
import Refcomp from './Refcomp';
import FRParent from './FRParent';
import Hero from './Hero';
import ErrorBoundryn from './ErrorBoundry';
import ClassCounter from './ClassCounter';
import HoverCounter from './HoverCounter';
import UseStateHook from './UseStateHook';
import UseEffectHook from './UseEffectHook';
import TodoUseStateHook from './TodoUseStateHook';
function App() {
  return (
    <div className="App">
      <header className="App-header">
     {/*<Card name="Prabhat"/>*/}
     {/* <Card name="Ankit"/>
      <Card name="Nafar"/>*/}
     {/* <Profile name="Prabhat" heroname="Captain America">
        <p>This Is A Paragraph</p>
      </Profile>
      <Profile name="ankit"  heroname="Loki"/>
      <button>Action</button>
      <Profile name="sadab"  heroname="Hulk"/> 
      <h2>This Heading Second</h2>
      <Profile info="NAFAR"  heroname="Iron Man"/>
      <p>This Is props Childrens </p>*/}

    { /* <Message></Message>     
    <Counter></Counter>     
    <Eventbind/>
    <ParentComponent */ }
    {/* <Inline/>
    <StyleSheet/>
    <Condition></Condition>
    <ListName></ListName> */}
    {/* <Form/> */}
    {/* <Fragmentex/> */}
    {/* <Table/> */}
    {/* <Parentcompo/> */}
    {/* <Refcomp></Refcomp> */}
    {/* <FRParent/> */}
    {/* <ErrorBoundryn>
        <Hero heroname="BataMAn"/>
        </ErrorBoundryn>
        <ErrorBoundryn>
    <Hero heroname="SuperMan"/>
    </ErrorBoundryn>
     <ErrorBoundryn>
    <Hero heroname="Joker"/>
    </ErrorBoundryn> */}
   {/* <ClassCounter/>
   <HoverCounter/> */}
   {/* <UseStateHook/> */}
   {/* <UseEffectHook/> */}
   <TodoUseStateHook/>
             </header>
           
    </div>
  );
}

export default App;
