import React, { Component } from 'react'

class Form extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             username:"",
             password:"",
             topic:""
        }
    }
    handleUserName=(event)=>{
        this.setState({
            username:event.target.value
         
        })
    }
            handleUserPassword=(event)=>{
                this.setState({
                    password:event.target.value
                })
            
        
    }
    handleSelect=(event)=>{
        this.setState({
            topic:event.target.value
        })
    }
    handleSubmit=(event)=>{
        alert( `${this.state.username},${this.state.password}, ${this.state.topic}`)
        event.preventDefault();
        console.log(this.state);
    }

render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div>
                    <label>UserName</label>
                    <input type="text" value={this.state.username} onChange={this.handleUserName}></input>
                    <div>
                    <label>Password</label>
                    <input type="Password" value={this.state.password} onChange={this.handleUserPassword}></input>
                    </div>
                    <div>
                        <label>Topic</label>
                        <select value={this.state.topic} onChange={this.handleSelect}>
                            <option value="react">React</option>
                            
                            <option value="Vue">Vue</option>
                            
                            <option value="Angular">Angular</option>

                        </select>
                    </div>

                   
                </div>
                    <button type="submit" >submit</button>
            </form>
        )
    }
}

export default Form;
