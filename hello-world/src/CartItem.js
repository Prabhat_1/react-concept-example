import React from 'react'

function CartItem({name,quantity,onClick,remove,price}) {
    return (
        <div className="CartItem">
         <span className="CartItem__label">{`${name}:` +`${price}`}</span>
         <div>
        <button className="CartItem__btn" onClick={remove}>
          -
        </button>
        <input className="CartItem__input" value={quantity} readOnly />
        <button className="CartItem__btn" onClick={onClick}>
          +
        </button>
      </div>
       
        </div>
    )
}

export default CartItem
