import React, { Component } from 'react'
import { Media ,Card,CardImg,CardText,CardBody,CardTitle, CardImgOverlay} from 'reactstrap';

export class Menucomponent extends Component {
    constructor(props){
        super(props);
        this.state={
            selectedDish:null
           
        };
        
    }
    OndishSelect(dish){
        this.setState({selectedDish:dish} )
    }
    renderDish(dish){
        if(dish!=null)
        {
                return(
                    <Card>
                            <CardImg src={dish.image} alt="no image" top width="100%"/>
                            <CardBody>
                                <CardTitle tag="h5">{dish.name}</CardTitle>
                                {<CardText>{dish.description}</CardText> }
                            </CardBody>
                        </Card>
                )
        }
        else{
            return(
                <div></div>
            )
        }
    }
    render() {
      
                const menu=this.props.setdish.map((dish)=>{
                    return (
                        <div key={dish.id} className="col-12 col-md-5 m-1">
                        <Card onClick={()=>this.OndishSelect(dish)}>
                            <CardImg src={dish.image} alt="no image" top width="100%"/>
                            <CardImgOverlay>
                                <CardTitle tag="h5">{dish.name}</CardTitle>
                                {/* <CardText>{dish.description}</CardText> */}
                            </CardImgOverlay>
                        </Card>
                        </div>
                    )
                })



        return (
            <div className="container">
            <div className="row">
             
                  {menu}
              
            </div>
            <div className="row">
                {this.renderDish(this.state.selectedDish)}
            </div>

          </div>
        )
    }
}

export default Menucomponent
