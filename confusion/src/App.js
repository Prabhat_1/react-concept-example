
import {Navbar, NavbarBrand} from 'reactstrap';
import Menucomponent from './component/Menucomponent';

import { dishes } from './shared/dishes';

function App() {
    
  return (
    <div className="App">
      <Navbar dark color="primary">
      <div className="container">
      <NavbarBrand href="/">Resturent the Fusion</NavbarBrand>
      </div>
      </Navbar>
      <Menucomponent setdish={dishes}/>
     
    </div>
  );
}

export default App;
